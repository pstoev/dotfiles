function parse_git_branch {
 # ref=$(git-symbolic-ref HEAD 2> /dev/null) || return
#	echo "("${ref#refs/heads/}")"
git branch --no-color 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/(\1)/'
}
# Colors from http://wiki.archlinux.org/index.php/Color_Bash_Prompt
# misc
NO_COLOR='\e[0m' #disable any colors
# regular colors
BLACK='\e[0;30m'
RED='\e[0;31m'
GREEN='\e[0;32m'
YELLOW='\e[0;33m'
BLUE='\e[0;34m'
MAGENTA='\e[0;35m'
CYAN='\e[0;36m'
WHITE='\e[0;37m'
# emphasized (bolded) colors
EBLACK='\e[1;30m'
ERED='\e[1;31m'
EGREEN='\e[1;32m'
EYELLOW='\e[1;33m'
EBLUE='\e[1;34m'
EMAGENTA='\e[1;35m'
ECYAN='\e[1;36m'
EWHITE='\e[1;37m'
# underlined colors
UBLACK='\e[4;30m'
URED='\e[4;31m'
UGREEN='\e[4;32m'
UYELLOW='\e[4;33m'
UBLUE='\e[4;34m'
UMAGENTA='\e[4;35m'
UCYAN='\e[4;36m'
UWHITE='\e[4;37m'
# background colors
BBLACK='\e[40m'
BRED='\e[41m'
BGREEN='\e[42m'
BYELLOW='\e[43m'
BBLUE='\e[44m'
BMAGENTA='\e[45m'
BCYAN='\e[46m'
BWHITE='\e[47m'

###########
# Aliases #
###########
alias g="git status"
alias ls="ls -A"
alias todo="echo $@ >> ~/TODO_LIST"

# Flush Directory Service cache
alias flush="dscacheutil -flushcache"

# View HTTP traffic
alias sniff="sudo ngrep -d 'en0' -t '^(GET|POST) ' 'tcp and port 80'"
alias httpdump="sudo tcpdump -i en0 -n -s 0 -w - | grep -a -o -E \"Host\: .*|GET \/.*\""

# Programs:
alias slt='open -a "Sublime Text 2"'

# List only directories
alias lsd='ls -l | grep "d"'

cat ~/TODO_LIST

# Update the command prompt to be <user>:<current_directory>(git_branch) >
# Note that the git branch is given a special color
PS1="\n\[$BLACK\]\u\[$BLACK\]:\[$RED\]\w \[$RED\]\[$BLACK\]\$(parse_git_branch) \[$NO_COLOR\]\n> "

##
# Your previous /Users/plamen_stoev/.bash_profile file was backed up as /Users/plamen_stoev/.bash_profile.macports-saved_2012-08-31_at_12:38:01
##

# MacPorts Installer addition on 2012-08-31_at_12:38:01: adding an appropriate PATH variable for use with MacPorts.
export PATH=/opt/local/bin:/opt/local/sbin:/opt/local/apache2/bin:$PATH
# Finished adapting your PATH environment variable for use with MacPorts.

