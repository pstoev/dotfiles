# .files

Here em go - dotfiles

### Sublime package sync:
Setup symlink files using Windows PowerShell for Dropbox. Latest instructions can be found [here](https://packagecontrol.io/docs/syncing).

#### First machine
```
cd "$env:appdata\Sublime Text 3\Packages\"  
mkdir $env:userprofile\Dropbox\Sublime  
mv User $env:userprofile\Dropbox\Sublime\  
cmd /c mklink /D User $env:userprofile\Dropbox\Sublime\User
```

#### Other machines:
```
cd "$env:appdata\Sublime Text 3\Packages\"
rmdir -recurse User
cmd /c mklink /D User $env:userprofile\Dropbox\Sublime\User
```