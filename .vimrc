set nocompatible
source $VIMRUNTIME/vimrc_example.vim
source $VIMRUNTIME/mswin.vim
behave mswin

" Show highlighted search:
set hlsearch

set diffexpr=MyDiff()
function MyDiff()
  let opt = '-a --binary '
  if &diffopt =~ 'icase' | let opt = opt . '-i ' | endif
  if &diffopt =~ 'iwhite' | let opt = opt . '-b ' | endif
  let arg1 = v:fname_in
  if arg1 =~ ' ' | let arg1 = '"' . arg1 . '"' | endif
  let arg2 = v:fname_new
  if arg2 =~ ' ' | let arg2 = '"' . arg2 . '"' | endif
  let arg3 = v:fname_out
  if arg3 =~ ' ' | let arg3 = '"' . arg3 . '"' | endif
  let eq = ''
  if $VIMRUNTIME =~ ' '
    if &sh =~ '\<cmd'
      let cmd = '""' . $VIMRUNTIME . '\diff"'
      let eq = '"'
    else
      let cmd = substitute($VIMRUNTIME, ' ', '" ', '') . '\diff"'
    endif
  else
    let cmd = $VIMRUNTIME . '\diff'
  endif
  silent execute '!' . cmd . ' ' . opt . arg1 . ' ' . arg2 . ' > ' . arg3 . eq
endfunction

" PS
" Default file reading encoding:
set fileencodings=utf-8,latin1,cp1251

" Set default encoding for typed-in characters before converting them to utf-8:
let &termencoding = &encoding
set encoding=utf-8 nobomb

" Set tab space
set tabstop=2

" set indent size
set shiftwidth=2

" replace tabs with spaces:
set expandtab

" Set to show line numbers
set number

" Set to show relative line numbers:
" set relativenumber

" Change default font:
if has('gui_running')
	set guifont=Lucida_Console:h10:cDEFAULT
endif

" Show .json files as .js:
if has('autocmd')
	filetype on
	autocmd BufNewFile,BufRead *.json setfiletype json syntax=javascript
endif
"as
" THEME:
"colorscheme darkmate
